import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики со страницей рейтингов котиков.
 *
 */
export class RatingPage {
  private page: Page;
  public errorSelector: string;
  public errorMessage: string;

  public tableSelector: string;
  public tableCountSelector: string;

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
    this.errorSelector = "//*[contains(@class,'ajs-message')]";
    this.errorMessage = "Ошибка загрузки рейтинга";
    this.tableSelector = "//*[contains(@class,'rating-names_table__')]";
    this.tableCountSelector = "//*[contains(@class,'rating-names_item-count__')]";
  }
  // количество таблиц на странице
  readonly TABLE_COUNT = 2;

  // количество строк в каждой таблице
  readonly TABLE_ROWS = 10;

  // путь к api rating
  readonly API_RATING = '/api/likes/cats/rating';

  async mockServerError() {
    await this.page.route(
      request => request.href.includes(this.API_RATING),
      async route => {
        await route.fulfill({
          status: 500,
        });
      }
    );
  }

  async openRatingPage() {
    return await test.step('Открываю страницу рейтинга котиков', async () => {
      await this.page.goto('/rating');
    });
  }

  async waitApiRating() {
    return await test.step('Ждем ответ от API', async () => {
      await this.page.waitForResponse(response => response.url().includes(this.API_RATING));
    });
  }

}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
  >;

export const ratingPageFixture: RatingPageFixture = async (
  { page },
  use
) => {
  const ratingPage = new RatingPage({ page });

  await use(ratingPage);
};


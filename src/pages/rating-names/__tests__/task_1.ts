import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('при ошибке сервера в методе rating - отображается попап ошибки', async ({ page, ratingPage }) => {
  /**
   * Замокать ответ метода получения рейтинга ошибкой на стороне сервера
   * Перейти на страницу рейтинга
   * Проверить, что отображается текст ошибка загрузки рейтинга
   */

  await ratingPage.mockServerError();
  await ratingPage.openRatingPage();

  // проверяем отображение блока ошибки
  const errorMessage = page.locator(ratingPage.errorSelector);
  await expect(errorMessage).toBeVisible();

  // проверяем текст ошибки
  await expect(await errorMessage.textContent()).toEqual(ratingPage.errorMessage);
});
import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

// названия тестов с маленькой буквы, т.к. на windows большая буква ломает подсказку (путь к архиву трейса - в результатах после теста)
test('рейтинг котиков отображается', async ({ page, ratingPage }) => {
  /**
   * Перейти на страницу рейтинга
   * Проверить, что рейтинг количества лайков отображается по убыванию
   */

  // открываем страницу
  await ratingPage.openRatingPage();

  // ждем api ответ
  await ratingPage.waitApiRating();

  // проверяем количество таблиц на странице
  const allTables = page.locator(ratingPage.tableSelector);
  await expect(allTables).toHaveCount(ratingPage.TABLE_COUNT);

  for (const tableLocator of await allTables.all()) {
    const allCounts = tableLocator.locator(ratingPage.tableCountSelector);
    
    // проверяем количество строк в каждой таблице
    await expect(allCounts).toHaveCount(ratingPage.TABLE_ROWS);

    // проверяем сортировку рейтингов
    let prevCount = undefined;
    for (const countLocator of await allCounts.all()) {
      let countTemp = parseInt(await countLocator.textContent());
      
      if (prevCount !== undefined)
      {
        expect(countTemp <= prevCount).toEqual(true);
      }
      prevCount = countTemp;
    }
  }
});